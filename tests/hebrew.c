
#include <libgnomeprint/gnome-print.h>
#include <libgnomeprint/gnome-print-job.h>
#include <libgnomeprint/gnome-print-config.h>
#include <libgnomeprint/gnome-print-pango.h>
#include <pango/pango-layout.h>

const char * text ="בְּרֵאשִׁית בָּרָא אֱלֹהִים אֵת הַשָּׁמַיִם וְאֵת הָאָרֶץ\n"
"Gn 1,2 וְהָאָרֶץ הָיְתָה תֹהוּ וָבֹהוּ וְחֹשֶׁךְ עַל־פְּנֵי תְהֹום וְרוּחַ אֱלֹהִים מְרַחֶפֶת עַל־פְּנֵי הַמָּיִם ";

const char* text1="בְּרֵאשִׁ֖ית בָּרָ֣א אֱלֹהִ֑ים אֵ֥ת הַשָּׁמַ֖יִם וְאֵ֥ת  הָאָֽרֶץ׃";

/*
static void
draw_hello_world (GnomePrintContext *gpc,
                  double             page_width,
                  double             page_height)
{
  PangoLayout *layout = gnome_print_pango_create_layout (gpc);
  int pango_width, pango_height;
  double width, height;

  pango_layout_set_text (layout, "Hello World");

  pango_layout_get_size (layout, &pango_width, &pango_height);
  width = (double) pango_width / PANGO_SCALE;
  height = (double) pango_height / PANGO_SCALE;

  gnome_print_moveto (gpc,
	             (page_width - width) / 2,
                     (page_width - height)/ 2);
  gnome_print_pango_layout (gpc, layout);

  g_object_unref (layout);
}*/

void main () {
 GnomePrintConfig * config;
 GnomePrintJob * job;
 GnomePrintContext * context;
 PangoLayout *layout;

  g_type_init();  // asi neni nutno, pokud je inicializovano gtk/glib...

  job = gnome_print_job_new (NULL);
  context = gnome_print_job_get_context (job);
  config = gnome_print_job_get_config (job);

  gnome_print_config_set(config, GNOME_PRINT_KEY_PAPER_SIZE, "A4");
  gnome_print_config_set(config, GNOME_PRINT_KEY_PREFERED_UNIT, "cm");
  gnome_print_config_set(config, GNOME_PRINT_KEY_DOCUMENT_NAME, "Test...");
  gnome_print_config_set(config, "Printer", "PDF");

  gnome_print_job_print_to_file(job, "test.pdf");


 /* start printing... */

  gnome_print_beginpage(context, "test1");

  gnome_print_setlinewidth(context, 6);
  gnome_print_setrgbcolor(context, 1,.7,0);

  gnome_print_newpath(context);

  gnome_print_moveto(context, 0, 100);
  gnome_print_lineto(context, 100, 100);
  gnome_print_lineto(context, 100, 200);
  gnome_print_lineto(context, 200, 200);

//  gnome_print_closepath(context);
//  gnome_print_strokepath(context);

  gnome_print_stroke(context);

/* pango-layout... */
  layout = gnome_print_pango_create_layout (context);
  pango_layout_set_text (layout, text1, -1);
  pango_layout_set_width (layout, 350 * PANGO_SCALE);

//  pango_layout_set_font_description (layout, pango_font_description_from_string("SBL Hebrew 16"));
  pango_layout_set_font_description (layout, pango_font_description_from_string("Ezra SIL 17"));

  gnome_print_newpath(context);
  gnome_print_moveto(context, 80,310);
  gnome_print_setrgbcolor(context, 0,0,0);
  gnome_print_pango_layout(context, layout);

  gnome_print_showpage(context);  

/* job... print...! */
  gnome_print_job_close (job);
  gnome_print_job_print (job);

}



