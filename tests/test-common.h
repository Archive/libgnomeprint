#ifndef __TEST_COMMON_H__
#define __TEST_COMMON_H__

#include <libgnomeprint/gnome-print.h>

void test_print_page (GnomePrintContext *pc, guint page);

#endif /* __TEST_COMMON_H__ */
