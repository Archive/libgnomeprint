# libgnomeprint eesti tõlge.
# Copyright (C) 2002 Free Software Foundation, Inc.
# Tõivo Leedjärv <toivo@linux.ee>, 2002.
# Priit Laes <amd@store20.com>, 2004, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: libgnomeprint\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-09-06 17:06+0200\n"
"PO-Revision-Date: 2005-01-29 21:41+0300\n"
"Last-Translator: Priit Laes <amd@tt.ee>\n"
"Language-Team: Estonian <gnome-et@linux.ee>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../data/globals.xml.in.h:1
msgid "1/3 A4"
msgstr "1/3 A4"

#: ../data/globals.xml.in.h:2
msgid "1/3 A5"
msgstr "1/3 A5"

#: ../data/globals.xml.in.h:3
msgid "1/4 A3"
msgstr "1/4 A3"

#: ../data/globals.xml.in.h:4
msgid "1/4 A4"
msgstr "1/4 A4"

#: ../data/globals.xml.in.h:5
msgid "1/8 A4"
msgstr "1/8 A4"

#: ../data/globals.xml.in.h:6
msgid "2 pages to 1"
msgstr "2 lk 1-l"

#: ../data/globals.xml.in.h:7
msgid "4 pages to 1"
msgstr "4 lk 1-l"

#: ../data/globals.xml.in.h:8
msgid "A0"
msgstr "A0"

#: ../data/globals.xml.in.h:9
msgid "A1"
msgstr "A1"

#: ../data/globals.xml.in.h:10
msgid "A10"
msgstr "A10"

#: ../data/globals.xml.in.h:11
msgid "A2"
msgstr "A2"

#: ../data/globals.xml.in.h:12
msgid "A3"
msgstr "A3"

#. FIXME: fill in all papers and units. (Lauris)
#. FIXME: load papers from file (Lauris)
#. FIXME: use some fancy unit program (Lauris)
#.
#. * WARNING! Do not mess up with that - we use hardcoded numbers for base units!
#.
#: ../data/globals.xml.in.h:13 ../libgnomeprint/gnome-print-paper.c:43
msgid "A4"
msgstr "A4"

#: ../data/globals.xml.in.h:14
msgid "A5"
msgstr "A5"

#: ../data/globals.xml.in.h:15
msgid "A6"
msgstr "A6"

#: ../data/globals.xml.in.h:16
msgid "A7"
msgstr "A7"

#: ../data/globals.xml.in.h:17
msgid "A8"
msgstr "A8"

#: ../data/globals.xml.in.h:18
msgid "A9"
msgstr "A9"

#: ../data/globals.xml.in.h:19
msgid "B0"
msgstr "B0"

#: ../data/globals.xml.in.h:20
msgid "B1"
msgstr "B1"

#: ../data/globals.xml.in.h:21
msgid "B10"
msgstr "B10"

#: ../data/globals.xml.in.h:22
msgid "B2"
msgstr "B2"

#: ../data/globals.xml.in.h:23
msgid "B3"
msgstr "B3"

#: ../data/globals.xml.in.h:24
msgid "B4"
msgstr "B4"

#: ../data/globals.xml.in.h:25
msgid "B5"
msgstr "B5"

#: ../data/globals.xml.in.h:26
msgid "B6"
msgstr "B6"

#: ../data/globals.xml.in.h:27
msgid "B7"
msgstr "B7"

#: ../data/globals.xml.in.h:28
msgid "B8"
msgstr "B8"

#: ../data/globals.xml.in.h:29
msgid "B9"
msgstr "B9"

#: ../data/globals.xml.in.h:30
msgid "C0"
msgstr "C0"

#: ../data/globals.xml.in.h:31
msgid "C1"
msgstr "C1"

#: ../data/globals.xml.in.h:32
msgid "C10"
msgstr "C10"

#: ../data/globals.xml.in.h:33
msgid "C2"
msgstr "C2"

#: ../data/globals.xml.in.h:34
msgid "C3"
msgstr "C3"

#: ../data/globals.xml.in.h:35
msgid "C4"
msgstr "C4"

#: ../data/globals.xml.in.h:36
msgid "C5"
msgstr "C5"

#: ../data/globals.xml.in.h:37
msgid "C6"
msgstr "C6"

#: ../data/globals.xml.in.h:38
msgid "C6/C5 Envelope"
msgstr "C6/C5 ümbrik"

#: ../data/globals.xml.in.h:39
msgid "C7"
msgstr "C7"

#: ../data/globals.xml.in.h:40
msgid "C8"
msgstr "C8"

#: ../data/globals.xml.in.h:41
msgid "C9"
msgstr "C9"

#: ../data/globals.xml.in.h:42
msgid "Custom"
msgstr "Oma"

#: ../data/globals.xml.in.h:43
msgid "DL Envelope"
msgstr "DL ümbrik"

#: ../data/globals.xml.in.h:44
msgid "Divided"
msgstr "Jagatud"

#: ../data/globals.xml.in.h:45
msgid "Envelope 6x9"
msgstr "Ümbik 6x9"

#: ../data/globals.xml.in.h:46
msgid "Envelope No10"
msgstr "Ümbrik nr10"

#: ../data/globals.xml.in.h:47
msgid "Folded"
msgstr "Volditud"

#: ../data/globals.xml.in.h:48
msgid "Landscape"
msgstr "Rõhtsalt"

#: ../data/globals.xml.in.h:49
msgid "Plain"
msgstr "Lihtne"

#: ../data/globals.xml.in.h:50
msgid "Portrait"
msgstr "Püstiselt"

#: ../data/globals.xml.in.h:51
msgid "Rotated 180 degrees"
msgstr "Pööratud 180 kraadi"

#: ../data/globals.xml.in.h:52
msgid "Rotated 270 degrees"
msgstr "Pööratud 270 kraadi"

#: ../data/globals.xml.in.h:53
msgid "Rotated 90 degrees"
msgstr "Pööratud 90 kraadi"

#: ../data/globals.xml.in.h:54
msgid "Straight"
msgstr "Otse"

#: ../data/globals.xml.in.h:55
msgid "US Executive"
msgstr "US täidesaatev"

#: ../data/globals.xml.in.h:56
msgid "US Legal"
msgstr "US juriidiline"

#: ../data/globals.xml.in.h:57
msgid "US Letter"
msgstr "US kiri"

#: ../data/globals.xml.in.h:58
msgid "Upside down landscape"
msgstr "Teistpidi rõhtsalt"

#: ../data/globals.xml.in.h:59
msgid "Upside down portrait"
msgstr "Teistpidi püsitselt"

#: ../data/printers/GENERIC.xml.in.h:1
msgid "Generic Postscript"
msgstr "Üldine postskript"

#: ../data/printers/PDF-WRITER.xml.in.h:1
msgid "Create a PDF document"
msgstr "Loo PDF dokument"

#: ../libgnomeprint/gnome-font-face.c:494
msgid "The quick brown fox jumps over the lazy dog."
msgstr "Kiire pruun rebane hüppas üle laisa koera."

#: ../libgnomeprint/gnome-print-job.c:144
msgid "Job Configuration"
msgstr "Töö sätted"

#: ../libgnomeprint/gnome-print-job.c:145
msgid "The configuration for the print job"
msgstr "Printimistöö sätted"

#: ../libgnomeprint/gnome-print-job.c:149
msgid "Context"
msgstr "Kaastekst"

#: ../libgnomeprint/gnome-print-job.c:150
msgid "The context for the print job"
msgstr "Printtöö kaastekst"

#. Do not insert any elements before/between first 4
#: ../libgnomeprint/gnome-print-unit.c:36
msgid "Unit"
msgstr "Ühik"

#: ../libgnomeprint/gnome-print-unit.c:36
msgid "Units"
msgstr "Ühikud"

#: ../libgnomeprint/gnome-print-unit.c:37
msgid "Point"
msgstr "Punkt"

#: ../libgnomeprint/gnome-print-unit.c:37
msgid "Pt"
msgstr "Pt"

#: ../libgnomeprint/gnome-print-unit.c:37
msgid "Points"
msgstr "punkti"

#: ../libgnomeprint/gnome-print-unit.c:37
msgid "Pts"
msgstr "Pts"

#: ../libgnomeprint/gnome-print-unit.c:38
msgid "Userspace unit"
msgstr "Kasutajaruumi ühik"

#: ../libgnomeprint/gnome-print-unit.c:38
msgid "User"
msgstr "Kasutaja"

#: ../libgnomeprint/gnome-print-unit.c:38
msgid "Userspace units"
msgstr "Kasutajaruumi ühikud"

#: ../libgnomeprint/gnome-print-unit.c:39
msgid "Pixel"
msgstr "Piksel"

#: ../libgnomeprint/gnome-print-unit.c:39
msgid "Px"
msgstr "Px"

#: ../libgnomeprint/gnome-print-unit.c:39
msgid "Pixels"
msgstr "pikslit"

#. You can add new elements from this point forward
#: ../libgnomeprint/gnome-print-unit.c:41
msgid "Percent"
msgstr "Protsent"

#: ../libgnomeprint/gnome-print-unit.c:41
msgid "%"
msgstr "%"

#: ../libgnomeprint/gnome-print-unit.c:41
msgid "Percents"
msgstr "Protsenti"

#: ../libgnomeprint/gnome-print-unit.c:42
msgid "Millimeter"
msgstr "Millimeeter"

#: ../libgnomeprint/gnome-print-unit.c:42
msgid "mm"
msgstr "mm"

#: ../libgnomeprint/gnome-print-unit.c:42
msgid "Millimeters"
msgstr "millimeetrit"

#: ../libgnomeprint/gnome-print-unit.c:43
msgid "Centimeter"
msgstr "Sentimeeter"

#: ../libgnomeprint/gnome-print-unit.c:43
msgid "cm"
msgstr "cm"

#: ../libgnomeprint/gnome-print-unit.c:43
msgid "Centimeters"
msgstr "sentimeetrit"

#: ../libgnomeprint/gnome-print-unit.c:44
msgid "Inch"
msgstr "Toll"

#: ../libgnomeprint/gnome-print-unit.c:44
msgid "in"
msgstr "in"

#: ../libgnomeprint/gnome-print-unit.c:44
msgid "Inches"
msgstr "tolli"

#: ../libgnomeprint/modules/cups/gnome-print-cups.c:398
msgid "on the weekend"
msgstr "nädalavahetusel"

#: ../libgnomeprint/modules/cups/gnome-print-cups.c:400
msgid "between midnight and 8 a.m."
msgstr "kesköö ja hommikul kell 8. vahelisel ajal"

#. gpa_option_item_new (node,"night",_("between 6 p.m. and 6 a.m."));
#: ../libgnomeprint/modules/cups/gnome-print-cups.c:402
msgid "between 6 p.m. and 6 a.m."
msgstr "õhtul kell 6. ja hommikul kell 6. vahelisel ajal"

#: ../libgnomeprint/modules/cups/gnome-print-cups.c:404
msgid "between 4 p.m. and midnight"
msgstr "õhtul kell 4. ja kesköö vahelisel ajal"

#: ../libgnomeprint/modules/cups/gnome-print-cups.c:405
msgid "between 6 a.m. and 6 p.m."
msgstr "hommikul kell 6. ja õhtul kell 6. vahelisel ajal"

#: ../libgnomeprint/modules/cups/gnome-print-cups.c:406
msgid "when manually released"
msgstr "kui käsitsi päästetud"

#: ../libgnomeprint/modules/cups/gnome-print-cups.c:407
msgid "immediately"
msgstr "koheselt"

#: ../libgnomeprint/modules/lpd/gnome-print-lpd.c:119
#, c-format
msgid "%s (via lpr)"
msgstr "%s (lpr kaudu)"
