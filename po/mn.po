# translation of libgnomeprint.HEAD.po to Mongolian
# translation of libgnomeprint.HEAD.po to mongolian
# This file is distributed under the same license as the PACKAGE package.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER.
# Sanlig Badral <badral@chinggis.com>, 2003.
# Sanlig Badral <Badral@openmn.org>, 2004.
#
msgid ""
msgstr ""
"Project-Id-Version: libgnomeprint.HEAD\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgno"
"meprint&component=general\n"
"POT-Creation-Date: 2009-04-17 15:35+0000\n"
"PO-Revision-Date: 2010-03-22 02:01+0200\n"
"Last-Translator: Badral <badral@openmn.org>\n"
"Language-Team: Mongolian <openmn-core@lists.sf.net>\n"
"Language: mn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Pootle 2.0.3\n"

#: ../data/globals.xml.in.h:1
msgid "1/3 A4"
msgstr "1/3 A4"

#: ../data/globals.xml.in.h:2
msgid "1/3 A5"
msgstr "1/3 A5"

#: ../data/globals.xml.in.h:3
msgid "1/4 A3"
msgstr "1/4 A3"

#: ../data/globals.xml.in.h:4
msgid "1/4 A4"
msgstr "1/4 A4"

#: ../data/globals.xml.in.h:5
msgid "1/8 A4"
msgstr "1/8 A4"

#: ../data/globals.xml.in.h:6
msgid "2 pages to 1"
msgstr "2 хуудас 1 дээр"

#: ../data/globals.xml.in.h:7
msgid "4 pages to 1"
msgstr "4 хуудас 1 дээр"

#: ../data/globals.xml.in.h:8
msgid "A0"
msgstr "A0"

#: ../data/globals.xml.in.h:9
msgid "A1"
msgstr "A1"

#: ../data/globals.xml.in.h:10
msgid "A10"
msgstr "A10"

#: ../data/globals.xml.in.h:11
msgid "A2"
msgstr "A20"

#: ../data/globals.xml.in.h:12
msgid "A3"
msgstr "A3"

#. FIXME: fill in all papers and units. (Lauris)
#. FIXME: load papers from file (Lauris)
#. FIXME: use some fancy unit program (Lauris)
#.
#. * WARNING! Do not mess up with that - we use hardcoded numbers for base units!
#.
#: ../data/globals.xml.in.h:13 ../libgnomeprint/gnome-print-paper.c:43
msgid "A4"
msgstr "A4"

#: ../data/globals.xml.in.h:14
msgid "A5"
msgstr "A5"

#: ../data/globals.xml.in.h:15
msgid "A6"
msgstr "A6"

#: ../data/globals.xml.in.h:16
msgid "A7"
msgstr "A7"

#: ../data/globals.xml.in.h:17
msgid "A8"
msgstr "A8"

#: ../data/globals.xml.in.h:18
msgid "A9"
msgstr "A9"

#: ../data/globals.xml.in.h:19
msgid "B0"
msgstr "B0"

#: ../data/globals.xml.in.h:20
msgid "B1"
msgstr "B1"

#: ../data/globals.xml.in.h:21
msgid "B10"
msgstr "B10"

#: ../data/globals.xml.in.h:22
msgid "B2"
msgstr "B2"

#: ../data/globals.xml.in.h:23
msgid "B3"
msgstr "B3"

#: ../data/globals.xml.in.h:24
msgid "B4"
msgstr "B4"

#: ../data/globals.xml.in.h:25
msgid "B5"
msgstr "B5"

#: ../data/globals.xml.in.h:26
msgid "B6"
msgstr "B6"

#: ../data/globals.xml.in.h:27
msgid "B7"
msgstr "B7"

#: ../data/globals.xml.in.h:28
msgid "B8"
msgstr "B8"

#: ../data/globals.xml.in.h:29
msgid "B9"
msgstr "B9"

#: ../data/globals.xml.in.h:30
msgid "C0"
msgstr "C0"

#: ../data/globals.xml.in.h:31
msgid "C1"
msgstr "C1"

#: ../data/globals.xml.in.h:32
msgid "C10"
msgstr "C10"

#: ../data/globals.xml.in.h:33
msgid "C2"
msgstr "C2"

#: ../data/globals.xml.in.h:34
msgid "C3"
msgstr "C3"

#: ../data/globals.xml.in.h:35
msgid "C4"
msgstr "C4"

#: ../data/globals.xml.in.h:36
msgid "C5"
msgstr "C5"

#: ../data/globals.xml.in.h:37
msgid "C6"
msgstr "C6"

#: ../data/globals.xml.in.h:38
msgid "C6/C5 Envelope"
msgstr "C6/C5 Дугтуй"

#: ../data/globals.xml.in.h:39
msgid "C7"
msgstr "C7"

#: ../data/globals.xml.in.h:40
msgid "C8"
msgstr "C8"

#: ../data/globals.xml.in.h:41
msgid "C9"
msgstr "C9"

#: ../data/globals.xml.in.h:42
msgid "Custom"
msgstr "Хэрэглэгч тод/х"

#: ../data/globals.xml.in.h:43
msgid "DL Envelope"
msgstr "DL Дугтуй"

# CHECK
#: ../data/globals.xml.in.h:44
msgid "Divided"
msgstr "Хуваасан"

#: ../data/globals.xml.in.h:45
msgid "Envelope 6x9"
msgstr "6x9-Дугтуй"

#: ../data/globals.xml.in.h:46
msgid "Envelope No10"
msgstr "No10-Дугтуй"

# CHECK
#: ../data/globals.xml.in.h:47
msgid "Folded"
msgstr "Эвхсэн"

#: ../data/globals.xml.in.h:48
msgid "Landscape"
msgstr "Хэвтээ"

# CHECK
#: ../data/globals.xml.in.h:49
msgid "Plain"
msgstr "Энгийн"

#: ../data/globals.xml.in.h:50
msgid "Portrait"
msgstr "Босоо"

#: ../data/globals.xml.in.h:51
msgid "Rotated 180 degrees"
msgstr "180 градус эргүүлэх"

#: ../data/globals.xml.in.h:52
msgid "Rotated 270 degrees"
msgstr "270 градус эргүүлэх"

#: ../data/globals.xml.in.h:53
msgid "Rotated 90 degrees"
msgstr "90 градус эргүүлэх"

# CHECK
#: ../data/globals.xml.in.h:54
msgid "Straight"
msgstr "Шулуун"

#: ../data/globals.xml.in.h:55
msgid "US Executive"
msgstr "US-executive"

#: ../data/globals.xml.in.h:56
msgid "US Legal"
msgstr "US-Legal"

#: ../data/globals.xml.in.h:57
msgid "US Letter"
msgstr "US-Letter"

#: ../data/globals.xml.in.h:58
msgid "Upside down landscape"
msgstr "Доош харсан хэвтээ"

#: ../data/globals.xml.in.h:59
msgid "Upside down portrait"
msgstr "Доош харсан босоо"

# CHECK
#: ../data/printers/GENERIC.xml.in.h:1
msgid "Generic Postscript"
msgstr "PostScript, ерөнхий"

#: ../data/printers/PDF-WRITER.xml.in.h:1
msgid "Create a PDF document"
msgstr "PDF-баримт үүсгэх"

#. xgettext: Notice that this sentence contains every letter
#. * of the English alphabet. This is a special sentence, called
#. * a 'Pangram' (see: http://en.wikipedia.org/wiki/Pangram).
#. * This sentence is used to demonstrate in a nice way how well
#. * a font in your language looks. It's an improvement from having
#. * just the alphabet of a language.
#. * Therefore, add a short sentence here in your language that
#. * demonstrates the letters of your alphabet.
#: ../libgnomeprint/gnome-font-face.c:502
msgid "The quick brown fox jumps over the lazy dog."
msgstr "Чихний чимэг болсон аялгуу сайхан монгол хэл."

#: ../libgnomeprint/gnome-print-job.c:146
msgid "Job Configuration"
msgstr "Даалгавар тохируулга"

#: ../libgnomeprint/gnome-print-job.c:147
msgid "The configuration for the print job"
msgstr "Хэвлэх даалгаварын тохиргоо"

#: ../libgnomeprint/gnome-print-job.c:151
msgid "Context"
msgstr "Хам бичвэр"

#: ../libgnomeprint/gnome-print-job.c:152
msgid "The context for the print job"
msgstr "Хэвлэх даалгаварын хам бичвэр"

#. Do not insert any elements before/between first 4
#: ../libgnomeprint/gnome-print-unit.c:36
msgid "Unit"
msgstr "Нэгж"

#: ../libgnomeprint/gnome-print-unit.c:36
msgid "Units"
msgstr "Нэгжүүд"

#: ../libgnomeprint/gnome-print-unit.c:37
msgid "Point"
msgstr "Цэг"

#: ../libgnomeprint/gnome-print-unit.c:37
msgid "Pt"
msgstr "Цэг"

#: ../libgnomeprint/gnome-print-unit.c:37
msgid "Points"
msgstr "Цэгүүд"

#: ../libgnomeprint/gnome-print-unit.c:37
msgid "Pts"
msgstr "Цэгс"

# CHECK
#: ../libgnomeprint/gnome-print-unit.c:38
msgid "Userspace unit"
msgstr "Хэрэглэгчийн-талбар нэгж"

#: ../libgnomeprint/gnome-print-unit.c:38
msgid "User"
msgstr "Хэрэглэгч"

#: ../libgnomeprint/gnome-print-unit.c:38
msgid "Userspace units"
msgstr "Хэрэглэгчийн талбарын нэгжүүд"

#: ../libgnomeprint/gnome-print-unit.c:39
msgid "Pixel"
msgstr "Цэг"

#: ../libgnomeprint/gnome-print-unit.c:39
msgid "Px"
msgstr "Цэг"

#: ../libgnomeprint/gnome-print-unit.c:39
msgid "Pixels"
msgstr "Цэгүүд"

#. You can add new elements from this point forward
#: ../libgnomeprint/gnome-print-unit.c:41
msgid "Percent"
msgstr "Хувь"

#: ../libgnomeprint/gnome-print-unit.c:41
msgid "%"
msgstr "%"

#: ../libgnomeprint/gnome-print-unit.c:41
msgid "Percents"
msgstr "Хувь"

#: ../libgnomeprint/gnome-print-unit.c:42
msgid "Millimeter"
msgstr "Миллиметр"

#: ../libgnomeprint/gnome-print-unit.c:42
msgid "mm"
msgstr "мм"

#: ../libgnomeprint/gnome-print-unit.c:42
msgid "Millimeters"
msgstr "Миллиметр"

#: ../libgnomeprint/gnome-print-unit.c:43
msgid "Centimeter"
msgstr "Сантиметр"

#: ../libgnomeprint/gnome-print-unit.c:43
msgid "cm"
msgstr "см"

#: ../libgnomeprint/gnome-print-unit.c:43
msgid "Centimeters"
msgstr "Сантиметр"

#: ../libgnomeprint/gnome-print-unit.c:44
msgid "Inch"
msgstr "Ямх"

#: ../libgnomeprint/gnome-print-unit.c:44
msgid "in"
msgstr "ямх"

#: ../libgnomeprint/gnome-print-unit.c:44
msgid "Inches"
msgstr "Ямх"

#: ../libgnomeprint/modules/cups/gnome-print-cups.c:398
msgid "on the weekend"
msgstr "Хагас бүтэн сайнаар"

#: ../libgnomeprint/modules/cups/gnome-print-cups.c:400
msgid "between midnight and 8 a.m."
msgstr "Шөнө дунд ба өглөөний 8-н хооронд"

#. gpa_option_item_new (node,"night",_("between 6 p.m. and 6 a.m."));
#: ../libgnomeprint/modules/cups/gnome-print-cups.c:402
msgid "between 6 p.m. and 6 a.m."
msgstr "18 ба 6 цагийн хооронд"

#: ../libgnomeprint/modules/cups/gnome-print-cups.c:404
msgid "between 4 p.m. and midnight"
msgstr "16 цаг ба шөнө дунд хооронд"

#: ../libgnomeprint/modules/cups/gnome-print-cups.c:405
msgid "between 6 a.m. and 6 p.m."
msgstr "6 ба 18 цагийн хооронд"

#: ../libgnomeprint/modules/cups/gnome-print-cups.c:406
msgid "when manually released"
msgstr "гараар"

#: ../libgnomeprint/modules/cups/gnome-print-cups.c:407
msgid "immediately"
msgstr "дариу"

#: ../libgnomeprint/modules/lpd/gnome-print-lpd.c:119
#, c-format
msgid "%s (via lpr)"
msgstr "%s (lpr -р)"
