# translation of libgnomeprint.HEAD.po to Punjabi
# This file is distributed under the same license as the PACKAGE package.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER.
#
#
# Amanpreet Singh Alam <amanliunx@netscapet.net>, 2004.
# Amanpreet Singh Alam <aalam@redhat.com>, 2004.
# Amanpreet Singh Alam <amanpreetalam@yahoo.com>, 2005.
# A S Alam <aalam@users.sf.net>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: libgnomeprint.HEAD\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2007-01-02 03:12+0000\n"
"PO-Revision-Date: 2007-02-25 18:29+0530\n"
"Last-Translator: A S Alam <aalam@users.sf.net>\n"
"Language-Team: Punjabi <punjabi-l10n@lists.sf.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"\n"

#: ../data/globals.xml.in.h:1
msgid "1/3 A4"
msgstr "1/3 A4"

#: ../data/globals.xml.in.h:2
msgid "1/3 A5"
msgstr "1/3 A5"

#: ../data/globals.xml.in.h:3
msgid "1/4 A3"
msgstr "1/4 A3"

#: ../data/globals.xml.in.h:4
msgid "1/4 A4"
msgstr "1/4 A4"

#: ../data/globals.xml.in.h:5
msgid "1/8 A4"
msgstr "1/8 A4"

#: ../data/globals.xml.in.h:6
msgid "2 pages to 1"
msgstr "2 ਸਫ਼ੇ ਤੋਂ 1"

#: ../data/globals.xml.in.h:7
msgid "4 pages to 1"
msgstr "4 ਸਫ਼ੇ ਤੋਂ 1"

#: ../data/globals.xml.in.h:8
msgid "A0"
msgstr "A0"

#: ../data/globals.xml.in.h:9
msgid "A1"
msgstr "A1"

#: ../data/globals.xml.in.h:10
msgid "A10"
msgstr "A10"

#: ../data/globals.xml.in.h:11
msgid "A2"
msgstr "A2"

#: ../data/globals.xml.in.h:12
msgid "A3"
msgstr "A3"

#. FIXME: fill in all papers and units. (Lauris)
#. FIXME: load papers from file (Lauris)
#. FIXME: use some fancy unit program (Lauris)
#.
#. * WARNING! Do not mess up with that - we use hardcoded numbers for base units!
#.
#: ../data/globals.xml.in.h:13 ../libgnomeprint/gnome-print-paper.c:43
msgid "A4"
msgstr "A4"

#: ../data/globals.xml.in.h:14
msgid "A5"
msgstr "A5"

#: ../data/globals.xml.in.h:15
msgid "A6"
msgstr "A6"

#: ../data/globals.xml.in.h:16
msgid "A7"
msgstr "A7"

#: ../data/globals.xml.in.h:17
msgid "A8"
msgstr "A8"

#: ../data/globals.xml.in.h:18
msgid "A9"
msgstr "A9"

#: ../data/globals.xml.in.h:19
msgid "B0"
msgstr "B0"

#: ../data/globals.xml.in.h:20
msgid "B1"
msgstr "B1"

#: ../data/globals.xml.in.h:21
msgid "B10"
msgstr "B10"

#: ../data/globals.xml.in.h:22
msgid "B2"
msgstr "B2"

#: ../data/globals.xml.in.h:23
msgid "B3"
msgstr "B3"

#: ../data/globals.xml.in.h:24
msgid "B4"
msgstr "B4"

#: ../data/globals.xml.in.h:25
msgid "B5"
msgstr "B5"

#: ../data/globals.xml.in.h:26
msgid "B6"
msgstr "B6"

#: ../data/globals.xml.in.h:27
msgid "B7"
msgstr "B7"

#: ../data/globals.xml.in.h:28
msgid "B8"
msgstr "B8"

#: ../data/globals.xml.in.h:29
msgid "B9"
msgstr "B9"

#: ../data/globals.xml.in.h:30
msgid "C0"
msgstr "C0"

#: ../data/globals.xml.in.h:31
msgid "C1"
msgstr "C1"

#: ../data/globals.xml.in.h:32
msgid "C10"
msgstr "C10"

#: ../data/globals.xml.in.h:33
msgid "C2"
msgstr "C2"

#: ../data/globals.xml.in.h:34
msgid "C3"
msgstr "C3"

#: ../data/globals.xml.in.h:35
msgid "C4"
msgstr "C4"

#: ../data/globals.xml.in.h:36
msgid "C5"
msgstr "C5"

#: ../data/globals.xml.in.h:37
msgid "C6"
msgstr "C6"

#: ../data/globals.xml.in.h:38
msgid "C6/C5 Envelope"
msgstr "C6/C5 ਲਿਫਾਫਾ"

#: ../data/globals.xml.in.h:39
msgid "C7"
msgstr "C7"

#: ../data/globals.xml.in.h:40
msgid "C8"
msgstr "C8"

#: ../data/globals.xml.in.h:41
msgid "C9"
msgstr "C9"

# libgnomeprint/gpa/gpa-media.c:241
#: ../data/globals.xml.in.h:42
msgid "Custom"
msgstr "ਚੋਣ"

#: ../data/globals.xml.in.h:43
msgid "DL Envelope"
msgstr "DL ਲਿਫਾਫਾ"

#: ../data/globals.xml.in.h:44
msgid "Divided"
msgstr "ਵੰਡੋ"

#: ../data/globals.xml.in.h:45
msgid "Envelope 6x9"
msgstr "ਲਿਫਾਫਾ 6x9"

#: ../data/globals.xml.in.h:46
msgid "Envelope No10"
msgstr "ਲਿਫਾਫਾ ਨੰ 10"

#: ../data/globals.xml.in.h:47
msgid "Folded"
msgstr "ਲਪੇਟਿਆ"

# libgnomeprint/gpa/gpa-media.c:310
#: ../data/globals.xml.in.h:48
msgid "Landscape"
msgstr "ਲੈਡਸਕੇਪ"

# libgnomeprint/gpa/gpa-media.c:343
#: ../data/globals.xml.in.h:49
msgid "Plain"
msgstr "ਪਲੇਨ"

# libgnomeprint/gpa/gpa-media.c:303
#: ../data/globals.xml.in.h:50
msgid "Portrait"
msgstr "ਪੋਰਟਰੇਟ"

# libgnomeprint/gpa/gpa-media.c:277
#: ../data/globals.xml.in.h:51
msgid "Rotated 180 degrees"
msgstr "180 ਡਿਗਰੀ ਤੇ ਘੰਮਾਇਆ"

# libgnomeprint/gpa/gpa-media.c:284
#: ../data/globals.xml.in.h:52
msgid "Rotated 270 degrees"
msgstr "270 ਡਿਗਰੀ ਤੇ ਘੰਮਾਇਆ"

# libgnomeprint/gpa/gpa-media.c:270
#: ../data/globals.xml.in.h:53
msgid "Rotated 90 degrees"
msgstr "90 ਡਿਗਰੀ ਤੇ ਘੰਮਾਇਆ"

# libgnomeprint/gpa/gpa-media.c:263
#: ../data/globals.xml.in.h:54
msgid "Straight"
msgstr "ਸਿੱਧਾ"

#: ../data/globals.xml.in.h:55
msgid "US Executive"
msgstr "ਅਮਰੀਕੀ ਐਗੀਕਿਊਟਿਵ"

#: ../data/globals.xml.in.h:56
msgid "US Legal"
msgstr "ਅਮਰੀਕੀ ਲੀਗਲ"

#: ../data/globals.xml.in.h:57
msgid "US Letter"
msgstr "ਅਮਰੀਕੀ ਪੱਤਰ"

# libgnomeprint/gpa/gpa-media.c:324
#: ../data/globals.xml.in.h:58
msgid "Upside down landscape"
msgstr "ਉਪਰਲਾ ਹੇਠਾਂ ਕਰਕੇ ਲੈਡਸਕੈਪ"

# libgnomeprint/gpa/gpa-media.c:317
#: ../data/globals.xml.in.h:59
msgid "Upside down portrait"
msgstr "ਉਪਰਲਾ ਹੇਠਾਂ ਕਰਕੇ ਪੋਰਟਰੇਟ"

#: ../data/printers/GENERIC.xml.in.h:1
msgid "Generic Postscript"
msgstr "ਸਧਾਰਨ ਪੋਸਟਸਕ੍ਰਿਪਟ"

#: ../data/printers/PDF-WRITER.xml.in.h:1
msgid "Create a PDF document"
msgstr "ਇੱਕ PDF ਦਸਤਾਵੇਜ਼ ਬਣਾਓ"

#. xgettext: Notice that this sentence contains every letter
#. * of the English alphabet. This is a special sentence, called
#. * a 'Pangram' (see: http://en.wikipedia.org/wiki/Pangram).
#. * This sentence is used to demonstrate in a nice way how well
#. * a font in your language looks. It's an improvement from having
#. * just the alphabet of a language.
#. * Therefore, add a short sentence here in your language that
#. * demonstrates the letters of your alphabet.
#: ../libgnomeprint/gnome-font-face.c:502
msgid "The quick brown fox jumps over the lazy dog."
msgstr "ਪੁਰਜਾ ਪੁਰਜਾ ਕਟ ਮਰੈ ਕਬਹੁੰ ਨਾ ਛਾਡੈ ਖੇਤੁ"

#: ../libgnomeprint/gnome-print-job.c:146
msgid "Job Configuration"
msgstr "ਕੰਮ ਸੰਰਚਨਾ"

#: ../libgnomeprint/gnome-print-job.c:147
msgid "The configuration for the print job"
msgstr "ਛਾਪਣ ਦੇ ਕੰਮ ਦੀ ਸੰਰਚਨਾ"

#: ../libgnomeprint/gnome-print-job.c:151
msgid "Context"
msgstr "ਸਮੱਗਰੀ"

#: ../libgnomeprint/gnome-print-job.c:152
msgid "The context for the print job"
msgstr "ਛਪਾਈ ਲਈ ਸਮੱਗਰੀ"

# libgnomeprint/gnome-print-paper.c:46
#. Do not insert any elements before/between first 4
#: ../libgnomeprint/gnome-print-unit.c:36
msgid "Unit"
msgstr "ਇਕਾਈ"

# libgnomeprint/gnome-print-paper.c:46
#: ../libgnomeprint/gnome-print-unit.c:36
msgid "Units"
msgstr "ਇਕਾਈ"

# libgnomeprint/gnome-print-paper.c:47
#: ../libgnomeprint/gnome-print-unit.c:37
msgid "Point"
msgstr "ਬਿੰਦੂ"

# libgnomeprint/gnome-print-paper.c:47
#: ../libgnomeprint/gnome-print-unit.c:37
msgid "Pt"
msgstr "Pt"

# libgnomeprint/gnome-print-paper.c:47
#: ../libgnomeprint/gnome-print-unit.c:37
msgid "Points"
msgstr "ਬਿੰਦੂ"

# libgnomeprint/gnome-print-paper.c:47
#: ../libgnomeprint/gnome-print-unit.c:37
msgid "Pts"
msgstr "Pts"

# libgnomeprint/gnome-print-paper.c:48
#: ../libgnomeprint/gnome-print-unit.c:38
msgid "Userspace unit"
msgstr "Userspace ਇਕਾਈ"

# libgnomeprint/gnome-print-paper.c:48
#: ../libgnomeprint/gnome-print-unit.c:38
msgid "User"
msgstr "ਉਪਭੋਗੀ"

# libgnomeprint/gnome-print-paper.c:48
#: ../libgnomeprint/gnome-print-unit.c:38
msgid "Userspace units"
msgstr "Userspace ਇਕਾਈ"

# libgnomeprint/gnome-print-paper.c:49
#: ../libgnomeprint/gnome-print-unit.c:39
msgid "Pixel"
msgstr "ਪਿਕਸਲ"

# libgnomeprint/gnome-print-paper.c:49
#: ../libgnomeprint/gnome-print-unit.c:39
msgid "Px"
msgstr "Px"

# libgnomeprint/gnome-print-paper.c:49
#: ../libgnomeprint/gnome-print-unit.c:39
msgid "Pixels"
msgstr "ਪਿਕਸਲ"

# libgnomeprint/gnome-print-paper.c:51
#. You can add new elements from this point forward
#: ../libgnomeprint/gnome-print-unit.c:41
msgid "Percent"
msgstr "ਫ਼ੀ-ਸਦੀ"

# libgnomeprint/gnome-print-paper.c:51
#: ../libgnomeprint/gnome-print-unit.c:41
msgid "%"
msgstr "%"

# libgnomeprint/gnome-print-paper.c:51
#: ../libgnomeprint/gnome-print-unit.c:41
msgid "Percents"
msgstr "ਫੀ-ਸਦੀ"

# libgnomeprint/gnome-print-paper.c:52
#: ../libgnomeprint/gnome-print-unit.c:42
msgid "Millimeter"
msgstr "ਮਿਲੀਮੀਟਰ"

# libgnomeprint/gnome-print-paper.c:52
#: ../libgnomeprint/gnome-print-unit.c:42
msgid "mm"
msgstr "mm"

# libgnomeprint/gnome-print-paper.c:52
#: ../libgnomeprint/gnome-print-unit.c:42
msgid "Millimeters"
msgstr "ਮਿਲੀਮੀਟਰ"

# libgnomeprint/gnome-print-paper.c:53
#: ../libgnomeprint/gnome-print-unit.c:43
msgid "Centimeter"
msgstr "ਸੈਂਟੀਮੀਟਰ"

# libgnomeprint/gnome-print-paper.c:53
#: ../libgnomeprint/gnome-print-unit.c:43
msgid "cm"
msgstr "cm"

# libgnomeprint/gnome-print-paper.c:53
#: ../libgnomeprint/gnome-print-unit.c:43
msgid "Centimeters"
msgstr "ਸੈਂਟੀਮੀਟਰ"

# libgnomeprint/gnome-print-paper.c:55
#: ../libgnomeprint/gnome-print-unit.c:44
msgid "Inch"
msgstr "ਇੰਚ"

# libgnomeprint/gnome-print-paper.c:55
#: ../libgnomeprint/gnome-print-unit.c:44
msgid "in"
msgstr "ਇੰ"

# libgnomeprint/gnome-print-paper.c:55
#: ../libgnomeprint/gnome-print-unit.c:44
msgid "Inches"
msgstr "ਇੰਚ"

#: ../libgnomeprint/modules/cups/gnome-print-cups.c:398
msgid "on the weekend"
msgstr "ਹਫਤੇ ਦੇ ਅਖੀਰੀ ਦਿਨ"

#: ../libgnomeprint/modules/cups/gnome-print-cups.c:400
msgid "between midnight and 8 a.m."
msgstr "ਅੱਧੀ ਰਾਤ ਤੋਂ ਸਵੇਰੇ 8 ਵਜੇ ਤੱਕ"

#. gpa_option_item_new (node,"night",_("between 6 p.m. and 6 a.m."));
#: ../libgnomeprint/modules/cups/gnome-print-cups.c:402
msgid "between 6 p.m. and 6 a.m."
msgstr "ਸੰਝ 6 ਵਜੇ ਤੋਂ ਸਵੇਰੇ 6 ਵਜੇ ਤੱਕ"

#: ../libgnomeprint/modules/cups/gnome-print-cups.c:404
msgid "between 4 p.m. and midnight"
msgstr "ਸੰਝ 4 ਵਜੇ ਤੋਂ ਅੱਧੀ ਰਾਤ ਤੱਕ"

#: ../libgnomeprint/modules/cups/gnome-print-cups.c:405
msgid "between 6 a.m. and 6 p.m."
msgstr "ਸਵੇਰੇ 6 ਵਜੇ ਤੋਂ ਸੰਝ 6 ਵਜੇ ਤੱਕ"

#: ../libgnomeprint/modules/cups/gnome-print-cups.c:406
msgid "when manually released"
msgstr "ਜਦੋਂ ਦਸਤੀ ਜਾਰੀ ਹੋਵੋਗਾ"

#: ../libgnomeprint/modules/cups/gnome-print-cups.c:407
msgid "immediately"
msgstr "ਤਰੁੰਤ"

#: ../libgnomeprint/modules/lpd/gnome-print-lpd.c:119
#, c-format
msgid "%s (via lpr)"
msgstr "%s (lpr ਰਾਹੀ)"

